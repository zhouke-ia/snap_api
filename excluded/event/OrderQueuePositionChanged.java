package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.Order;

public class OrderQueuePositionChanged extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 312;
  private static final MsgType MSG_TYPE = MsgType.REP_QUEUE_POS;
  private Order order;

  public OrderQueuePositionChanged() {
    super(MSG_TYPE, MSG_LENGTH);
  }

}
