package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.MarketDataItem;
import com.integeralpha.api.snap.type.MsgType;

public class Volume extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 104;
  private static final MsgType MSG_TYPE = MsgType.REP_VOL;
  private long instrumentId;
  private MarketDataItem item;

  public Volume() {
    super(MSG_TYPE, MSG_LENGTH);
  }
}
