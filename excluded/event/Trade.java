package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.ExecReport;
import com.integeralpha.api.snap.type.MsgType;

public class Trade extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 368;
  private static final MsgType MSG_TYPE = MsgType.REP_TRADE;
  private ExecReport execReport;

  public Trade() {
    super(MSG_TYPE, MSG_LENGTH);
  }
}
