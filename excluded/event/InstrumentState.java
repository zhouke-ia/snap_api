package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;

public class InstrumentState extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 48;
  private static final MsgType MSG_TYPE = MsgType.REP_INSTRUMENT_STATE;
  private long instrumentId;
  private com.integeralpha.api.snap.type.InstrumentState state;

  public InstrumentState() {
    super(MSG_TYPE, MSG_LENGTH);
  }
}
