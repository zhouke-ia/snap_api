package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.MarketDataItem;
import com.integeralpha.api.snap.type.MsgType;

public class Open extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 104;
  private static final MsgType MSG_TYPE = MsgType.REP_OPEN;
  private long instrumentId;
  private MarketDataItem item;

  public Open() {
    super(MSG_TYPE, MSG_LENGTH);
  }
}
