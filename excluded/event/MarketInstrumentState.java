package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.Market;
import com.integeralpha.api.snap.type.MsgType;

public class MarketInstrumentState extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 40;
  private static final MsgType MSG_TYPE = MsgType.REP_INIT_INSTRUMENT_STATE_MARKET;
  private Market market;
  private com.integeralpha.api.snap.type.InstrumentState state;

  public MarketInstrumentState() {
    super(MSG_TYPE, MSG_LENGTH);
  }
}
