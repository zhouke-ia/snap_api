//package com.integeralpha.api.snap.message.reply;
//
//import com.google.auto.service.AutoService;
//import com.google.common.io.LittleEndianDataInputStream;
//import com.integeralpha.api.snap.message.AbstractIncomingMessage;
//import com.integeralpha.api.snap.message.SnapIncomingMessage;
//import com.integeralpha.api.snap.type.MsgType;
//import com.integeralpha.api.snap.type.Order;
//
//@AutoService(SnapIncomingMessage.class)
//public class InitialOrder extends AbstractIncomingMessage {
//
//  private static final int MSG_LENGTH = 312;
//  private static final MsgType MSG_TYPE = MsgType.REP_INIT_ORDER;
//  private Order order;
//
//  @Override
//  public MsgType getMsgType() {
//    return MSG_TYPE;
//  }
//
//  @Override
//  public void readFromInputStream(LittleEndianDataInputStream is) {
//
//  }
//}
