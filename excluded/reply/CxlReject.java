package com.integeralpha.api.snap.message.reply;

import com.integeralpha.api.snap.message.AbstractOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.Order;

public class CxlReject extends AbstractOutgoingMessage {

  public static final int MSG_LENGTH = 592;
  private static final MsgType MSG_TYPE = MsgType.REP_CXL_REJ;
  private Order cxlOrder;
  private Order originalOrder;

  public CxlReject() {
    super(MSG_TYPE, MSG_LENGTH);
  }
}
