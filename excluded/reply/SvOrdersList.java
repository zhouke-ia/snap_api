package com.integeralpha.api.snap.message.reply;

import com.integeralpha.api.snap.message.AbstractOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;

public class SvOrdersList extends AbstractOutgoingMessage {

  public static final int MSG_LENGTH = 48;
  private static final MsgType MSG_TYPE = MsgType.REP_SV_LIST;
  private long requestId;
  private long numberOfSvOrders;

  public SvOrdersList() {
    super(MSG_TYPE, MSG_LENGTH);
  }

}
