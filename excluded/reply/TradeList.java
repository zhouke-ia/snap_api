package com.integeralpha.api.snap.message.reply;

import com.integeralpha.api.snap.message.AbstractOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;

public class TradeList extends AbstractOutgoingMessage {

  public static final int MSG_LENGTH = 48;
  private static final MsgType MSG_TYPE = MsgType.REP_TRADES;
  private long requestId;
  private long numberOfTrades;

  public TradeList() {
    super(MSG_TYPE, MSG_LENGTH);
  }

}
