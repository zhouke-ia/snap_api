package com.integeralpha.api.snap.message.reply;

import com.integeralpha.api.snap.message.AbstractOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.PositionReport;

public class InitialPosition extends AbstractOutgoingMessage {

  public static final int MSG_LENGTH = 64;
  private static final MsgType MSG_TYPE = MsgType.REP_INIT_POS;
  private PositionReport positionReport;

  public InitialPosition() {
    super(MSG_TYPE, MSG_LENGTH);
  }

}
