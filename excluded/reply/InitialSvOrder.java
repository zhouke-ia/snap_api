package com.integeralpha.api.snap.message.reply;

import com.integeralpha.api.snap.type.MsgType;

public class InitialSvOrder extends SvOrderReport {

  private static final MsgType MSG_TYPE = MsgType.REP_INIT_SV;

  public InitialSvOrder() {
    super(MSG_TYPE);
  }
}
