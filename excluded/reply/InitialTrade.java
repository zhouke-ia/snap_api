package com.integeralpha.api.snap.message.reply;

import com.integeralpha.api.snap.message.AbstractOutgoingMessage;
import com.integeralpha.api.snap.type.ExecReport;
import com.integeralpha.api.snap.type.MsgType;

public class InitialTrade extends AbstractOutgoingMessage {

  public static final int MSG_LENGTH = 368;
  private static final MsgType MSG_TYPE = MsgType.REP_INIT_TRADE;
  private ExecReport execReport;

  public InitialTrade() {
    super(MSG_TYPE, MSG_LENGTH);
  }

}
