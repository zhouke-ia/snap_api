package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.google.common.io.LittleEndianDataInputStream;
import com.integeralpha.api.snap.message.AbstractIncomingMessage;
import com.integeralpha.api.snap.message.IncomingMessage;
import com.integeralpha.api.snap.type.MsgType;
import java.io.IOException;

@AutoService(IncomingMessage.class)
public class OrderList extends AbstractIncomingMessage {

  private static final int MSG_LENGTH = 56;
  private static final MsgType MSG_TYPE = MsgType.REP_ORDERS;
  private long requestId;
  private long numberOfOrders;
  private long lastSequenceNumber;

  public long getRequestId() {
    return requestId;
  }

  public long getNumberOfOrders() {
    return numberOfOrders;
  }

  public long getLastSequenceNumber() {
    return lastSequenceNumber;
  }

  @Override
  public MsgType getMsgType() {
    return MSG_TYPE;
  }

  @Override
  public void readFromInputStream(LittleEndianDataInputStream is) throws IOException {
    super.processHeader(is, MSG_LENGTH);
    this.requestId = is.readLong();
    this.numberOfOrders = is.readLong();
    this.lastSequenceNumber = is.readLong();
  }

  @Override
  public String toString() {
    return "OrderList{" +
        "requestId=" + requestId +
        ", numberOfOrders=" + numberOfOrders +
        ", lastSequenceNumber=" + lastSequenceNumber +
        '}';
  }
}
