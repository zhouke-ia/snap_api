package com.integeralpha.api.snap.message.reply;

import com.integeralpha.api.snap.message.AbstractOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.Order;

public class RplReject extends AbstractOutgoingMessage {

  public static final int MSG_LENGTH = 592;
  private static final MsgType MSG_TYPE = MsgType.REP_RPL_REJ;
  private Order rplOrder;
  private Order originalOrder;

  public RplReject() {
    super(MSG_TYPE, MSG_LENGTH);
  }
}
