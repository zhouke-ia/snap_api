package com.integeralpha.api.snap.message.request;

import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.Instrument;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.Side;

public class SvOrder extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 304;
  private static final MsgType MSG_TYPE = MsgType.REQ_NEW_SV;
  private Instrument triggerInstrument;
  private Side triggerSide;
  private double triggerPrice;
  private long triggerQuantity;
  private long sequenceNumber;
  private Order order;

  public SvOrder() {
    super(MSG_TYPE, MSG_LENGTH);
  }
}
