package com.integeralpha.api.snap.message.request;

import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;

public class CancelSvOrder extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 40;
  private static final MsgType MSG_TYPE = MsgType.REQ_CXL_SV;
  private long sequenceNumber;

  public CancelSvOrder() {
    super(MSG_TYPE, MSG_LENGTH);
  }
}
