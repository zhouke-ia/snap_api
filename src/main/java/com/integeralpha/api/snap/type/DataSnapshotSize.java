package com.integeralpha.api.snap.type;

enum DataSnapshotSize {
  ;

  private static final short BBO = 2;
  private static final short TRADE = 1;
  private static final short DEPTH = 10;
  private static final short STA = 2;
  private static final short EQI = 2;

  //TODO: What's BBO, STA, and EQI?

  public static short get(final MsgType msgType) {
    switch (msgType) {
      case REP_BBO_SNAPSHOT:
        return DataSnapshotSize.BBO;
      case REP_TRD_SNAPSHOT:
        return DataSnapshotSize.TRADE;
      case REP_DEPTH_SNAPSHOT:
        return DataSnapshotSize.DEPTH;
      case REP_STA_SNAPSHOT:
        return DataSnapshotSize.STA;
      case REP_EQI_SNAPSHOT:
        return DataSnapshotSize.EQI;
      default:
        throw new IllegalArgumentException("Illegal MsgType" + msgType.name());
    }
  }
}
