package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum InstrumentType {
  STOCK(0),
  FUTURE(1),
  OPTION(2),
  MLEG(5),
  WARRANT(6),
  CB(7); // TODO What's this?

  private static final Map<Integer, InstrumentType> lookupMap
      = Maps.newHashMapWithExpectedSize(InstrumentType.values().length);

  static {
    for (final InstrumentType type : InstrumentType.values()) {
      InstrumentType.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  InstrumentType(final int value) {
    this.value = value;
  }

  public static InstrumentType fromValue(final int value) {
    return InstrumentType.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
