package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum Side {
  BUY(1),
  SELL(2);
  private static final Map<Integer, Side> lookupMap
      = Maps.newHashMapWithExpectedSize(Side.values().length);

  static {
    for (final Side type : Side.values()) {
      Side.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  Side(final int value) {
    this.value = value;
  }

  public static Side fromValue(final int value) {
    return Side.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
