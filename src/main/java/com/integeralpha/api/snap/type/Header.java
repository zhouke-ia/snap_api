package com.integeralpha.api.snap.type;


import com.google.auto.value.AutoValue;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.integeralpha.api.snap.util.ByteStreamProcessor;
import java.io.IOException;

@SuppressWarnings("ClassWithTooManyDependents")
// From SNAP API
@AutoValue
public abstract class Header implements Transmittable {

  // private static final int HEADER_LENGTH = 32;

  private static final int MILLI_TO_NANO = 1000000;

  /**
   * This method returns a header for the given type and method length that is intended for
   * <b><i>outgoing</i></b> messages.
   * The message time accuracy is limited to millisecond level as we are not interested in doing
   * sub-millisecond HFT anyway
   *
   * @param type The message type shall be those values for outgoing messages
   * @param length Usually every SNAP API class has its length recorded.
   * @return the desired header for outgoing SNAP messages
   */
  public static Header create(final MsgType type, final long length) {
    return new AutoValue_Header(type, length, System.currentTimeMillis() * Header.MILLI_TO_NANO);
  }

  private static Header create(final MsgType type, final long length, final long time) {
    return new AutoValue_Header(type, length, time);
  }

  public static Header read(final LittleEndianDataInputStream is) throws IOException {
    final Header header = Header.create(MsgType.fromValue(is.readLong()),
                                        is.readLong(),
                                        is.readLong());
    is.skipBytes(8);
    return header;
  }

  public abstract MsgType getType();

  public abstract long getLength();

  public abstract long getTime();

  /**
   * Write the header to given output stream.
   */
  @SuppressWarnings("LawOfDemeter")
  // Must work this way by using Auto-Value
  public final void write(final LittleEndianDataOutputStream os) throws IOException {
    os.writeLong(getType().getValue());
    os.writeLong(getLength());
    os.writeLong(getTime());
    ByteStreamProcessor.fillZero(os, 8);
  }
}
