package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

@SuppressWarnings("StaticMethodOnlyUsedInOneClass")
public enum TimeInForce {
  DAY(1),
  GTC(2),
  GTD(3),
  IOC(4),
  FOK(5),
  FAK(6);

  private static final Map<Integer, TimeInForce> lookupMap
      = Maps.newHashMapWithExpectedSize(TimeInForce.values().length);

  static {
    for (final TimeInForce type : TimeInForce.values()) {
      TimeInForce.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  TimeInForce(final int value) {
    this.value = value;
  }

  public static TimeInForce fromValue(final int value) {
    return TimeInForce.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
