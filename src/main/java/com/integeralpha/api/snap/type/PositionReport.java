package com.integeralpha.api.snap.type;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

@SuppressWarnings("StaticMethodOnlyUsedInOneClass")
@AutoValue
public abstract class PositionReport {

  protected PositionReport() {}

  public static TypeAdapter<PositionReport> typeAdapter(final Gson gson) {
    return new AutoValue_PositionReport.GsonTypeAdapter(gson);
  }

  public abstract long instrumentId();

  public abstract double price();

  public abstract long qty();

  public abstract long traderId();
}
