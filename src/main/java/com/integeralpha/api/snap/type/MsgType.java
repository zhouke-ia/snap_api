package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

@SuppressWarnings("ClassWithTooManyDependents")
// from SNAP API, considered fixed once deployed
public enum MsgType {
  // TODO: verify the meaning and nature of the fields
  REQ_SUBSCRIBE(1),
  REP_SUBSCRIBE(2),
  MARKET_DATA(3),                       // verify whether these two are REQ or REP
  MARKET_DEPTH(4),
  REP_BBO_SNAPSHOT(5),
  REP_TRD_SNAPSHOT(6),
  REP_DEPTH_SNAPSHOT(7),
  REP_ORD_SNAPSHOT(8),
  REP_STA_SNAPSHOT(9),
  REP_EQI_SNAPSHOT(10),
  REP_INSTRUMENT_STATE(11),
  REQ_LOGIN(12),
  REQ_ORDERS(13),
  REP_ORDERS(14),
  REP_TRADES(15),
  REQ_TRADES(16),
  REQ_NEW_ORDER(17),
  REQ_RPL(18),
  REQ_CXL(19),
  REP_ACK(20),
  REP_FILL(21),
  REP_PART(22),
  REP_CXL(23),
  REP_RPL(24),
  REP_REJ(25),
  REP_CXL_REJ(26),
  REP_RPL_REJ(27),
  REP_QUEUE_POS(28),
  REP_TRADE(29),
  REP_POSITION(30),
  REP_INIT_ORDER(31),
  REP_INIT_TRADE(32),
  REP_INIT_INSTRUMENT_STATE_MARKET(33),
  REP_OPEN(34),
  REP_CLOSE(35),
  REP_VOL(36),
  REQ_NEW_SV(37),                       // what's exactly an SV order?
  REQ_CXL_SV(38),
  REQ_RPL_SV(39),
  REP_SV_ACTIVE(40),
  REP_SV_TRIGGERED(41),
  REP_SV_CXL(42),
  REP_SV_RPL(43),
  REP_SV_REJ(44),
  REP_SV_CXL_REJ(45),
  REP_SV_RPL_REJ(46),
  REQ_SV_LIST(47),
  REP_SV_LIST(48),
  REP_INIT_SV(49),
  REQ_POS(50),
  REP_POS_LIST(51),
  REP_INIT_POS(52);

  private static final Map<Integer, MsgType> lookupMap
      = Maps.newHashMapWithExpectedSize(MsgType.values().length);

  static {
    for (final MsgType type : MsgType.values()) {
      MsgType.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  MsgType(final int value) {
    this.value = value;
  }

  @SuppressWarnings("NumericCastThatLosesPrecision")
  // 52 possible values
  public static MsgType fromValue(final long value) {
    return MsgType.lookupMap.get((int) value);
  }

  public int getValue() {
    return value;
  }
}
