package com.integeralpha.api.snap.type;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

@SuppressWarnings("StaticMethodOnlyUsedInOneClass")
@AutoValue
public abstract class MarketDataItem {

  public static TypeAdapter<MarketDataItem> typeAdapter(final Gson gson) {
    return new AutoValue_MarketDataItem.GsonTypeAdapter(gson);
  }

  public abstract long instrumentId();

  public abstract DataType type();

  public abstract long price();

  public abstract long size();

  public abstract long number();

  //TODO: There are 3 more place holder members in original C API. Make sure they are redundant.


}
