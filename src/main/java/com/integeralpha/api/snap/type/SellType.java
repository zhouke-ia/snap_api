package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum SellType {  //TODO: check how the C API handles buy orders
  NONE(1),
  SELL_SHORT(2),
  SELL_EXEMPT(3);

  private static final Map<Integer, SellType> lookupMap
      = Maps.newHashMapWithExpectedSize(SellType.values().length);

  static {
    for (final SellType type : SellType.values()) {
      SellType.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  SellType(final int value) {
    this.value = value;
  }

  public static SellType fromValue(final int value) {
    return SellType.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
