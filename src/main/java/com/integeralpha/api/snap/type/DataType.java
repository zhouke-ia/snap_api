package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum DataType {
  BID(1),
  ASK(2),
  TRADE(3),
  BID_ORDER(4),
  ASK_ORDER(5),
  HIGH(6),
  LOW(7),
  EQ(8),        // TODO: What's this?
  OPEN(9),
  CLOSE(10),
  VOLUME(11);

  private static final Map<Integer, DataType> lookupMap
      = Maps.newHashMapWithExpectedSize(DataType.values().length);

  static {
    for (final DataType type : DataType.values()) {
      DataType.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  DataType(final int value) {
    this.value = value;
  }

  public static DataType fromValue(final int value) {
    return DataType.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
