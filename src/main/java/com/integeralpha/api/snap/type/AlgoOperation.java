package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum AlgoOperation {
  START(0),
  PAUSE(1),
  STOP(2);

  private static final Map<Integer, AlgoOperation> lookupMap
      = Maps.newHashMapWithExpectedSize(AlgoOperation.values().length);

  static {
    for (final AlgoOperation type : AlgoOperation.values()) {
      AlgoOperation.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  AlgoOperation(final int value) {
    this.value = value;
  }

  public static AlgoOperation fromValue(final int value) {
    return AlgoOperation.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
