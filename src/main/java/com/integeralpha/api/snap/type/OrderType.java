package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum OrderType {
  LIMIT(1),
  MARKET(2),
  ALGO(3),
  MIDPOINT(4),
  STOP(5),
  STOP_LIMIT(6);

  private static final Map<Integer, OrderType> lookupMap
      = Maps.newHashMapWithExpectedSize(OrderType.values().length);

  static {
    for (final OrderType type : OrderType.values()) {
      OrderType.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  OrderType(final int value) {
    this.value = value;
  }

  public static OrderType fromValue(final int value) {
    return OrderType.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
