package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum OrderState {
  PENDING(1),
  ACK(2),
  PARTIAL(3),
  FILL(4),
  PENDING_CXL(5),
  CXL(6),
  PENDING_RPL(7),
  PRL(8),
  REJ(9),
  SNAP_REJ(10),
  CXL_REJ(11),
  RPL_REJ(12),
  SNAP_RPL(13),
  EXPIRED(14); //TODO: verify this

  private static final Map<Integer, OrderState> lookupMap
      = Maps.newHashMapWithExpectedSize(OrderState.values().length);

  static {
    for (final OrderState type : OrderState.values()) {
      OrderState.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  OrderState(final int value) {
    this.value = value;
  }

  public static OrderState fromValue(final int value) {
    return OrderState.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
