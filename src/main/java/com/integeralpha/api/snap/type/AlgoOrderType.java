package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum AlgoOrderType {
  SV(0),
  CROSSING(1),
  TEST1(2),
  BATCH(3);

  private static final Map<Integer, AlgoOrderType> lookupMap
      = Maps.newHashMapWithExpectedSize(AlgoOrderType.values().length);

  static {
    for (final AlgoOrderType type : AlgoOrderType.values()) {
      AlgoOrderType.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  AlgoOrderType(final int value) {
    this.value = value;
  }

  public static AlgoOrderType fromValue(final int value) {
    return AlgoOrderType.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
