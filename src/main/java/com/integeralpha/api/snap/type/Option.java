package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum Option {
  // Mapped to long in C
  CALL(0),
  PUT(1);

  private static final Map<Integer, Option> lookupMap
      = Maps.newHashMapWithExpectedSize(Option.values().length);

  static {
    for (final Option type : Option.values()) {
      Option.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  Option(final int value) {
    this.value = value;
  }

  public static Option fromValue(final int value) {
    return Option.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
