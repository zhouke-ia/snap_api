package com.integeralpha.api.snap.type;

import com.google.gson.TypeAdapterFactory;
import com.ryanharter.auto.value.gson.GsonTypeAdapterFactory;


@GsonTypeAdapterFactory
public abstract class SnapGsonAdapterFactory implements TypeAdapterFactory {

  SnapGsonAdapterFactory() {}

  public static TypeAdapterFactory create() {
    return new AutoValueGson_SnapGsonAdapterFactory();
  }
}
