package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum ExecType {
  TRADE(0),
  ORDER_STATUS(1),
  RPL(2),
  AMEND(3),
  BUST(4); // TODO: verify this

  private static final Map<Integer, ExecType> lookupMap
      = Maps.newHashMapWithExpectedSize(ExecType.values().length);

  static {
    for (final ExecType type : ExecType.values()) {
      ExecType.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  ExecType(final int value) {
    this.value = value;
  }

  public static ExecType fromValue(final int value) {
    return ExecType.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
