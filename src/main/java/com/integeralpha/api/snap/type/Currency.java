package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum Currency {
  AUD(0),
  CAD(1),
  CHF(2),
  CNH(3),
  CZK(4),
  DKK(5),
  EUR(6),
  GBP(7),
  HKD(8),
  HUF(9),
  JPY(10),
  MXN(11),
  NOK(12),
  NZD(13),
  PLN(14),
  RUB(15),
  SEK(16),
  SGD(17),
  THB(18),
  TRY(19),
  USD(20),
  ZAR(21),
  CNY(22);

  private static final Map<Integer, Currency> lookupMap
      = Maps.newHashMapWithExpectedSize(Currency.values().length);

  static {
    for (final Currency type : Currency.values()) {
      Currency.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  Currency(final int value) {
    this.value = value;
  }

  public static Currency fromValue(final int value) {
    return Currency.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
