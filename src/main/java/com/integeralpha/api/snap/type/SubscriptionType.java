package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum SubscriptionType {
  L1(0), // Top of Book
  L2(1), // Depth Data
  L3(2); // Full trade book

  private static final Map<Integer, SubscriptionType> lookupMap
      = Maps.newHashMapWithExpectedSize(SubscriptionType.values().length);

  static {
    for (final SubscriptionType type : SubscriptionType.values()) {
      SubscriptionType.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  SubscriptionType(final int value) {
    this.value = value;
  }

  public static SubscriptionType fromValue(final int value) {
    return SubscriptionType.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
