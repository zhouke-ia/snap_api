package com.integeralpha.api.snap.type;

import com.google.auto.value.AutoValue;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.integeralpha.api.snap.util.ByteStreamProcessor;
import java.io.IOException;

/**
 *
 */
@SuppressWarnings("StaticMethodOnlyUsedInOneClass")
@AutoValue
public abstract class Instrument implements Transmittable {
  // length = 76

  private static final int MAX_SYMBOL_LENGTH = 63;

  Instrument() { }

  public static Instrument create(
      final String symbol, final Market market, final Currency currency, final InstrumentType type) {
    if (symbol.length() > Instrument.MAX_SYMBOL_LENGTH) {
      throw new IllegalArgumentException("Symbol length must be shorter than " +
                                         Instrument.MAX_SYMBOL_LENGTH);
    }
    return new AutoValue_Instrument(symbol, market, currency, type);
  }

  public static TypeAdapter<Instrument> typeAdapter(final Gson gson) {
    return new AutoValue_Instrument.GsonTypeAdapter(gson);
  }

  public static Instrument read(final LittleEndianDataInputStream is) throws IOException {
    return new AutoValue_Instrument(
        ByteStreamProcessor.readString(is, Instrument.MAX_SYMBOL_LENGTH + 1),
        Market.fromValue(is.readInt()),
        Currency.fromValue(is.readInt()),
        InstrumentType.fromValue(is.readInt()));
  }

  public abstract String symbol();

  public abstract Market market();

  public abstract Currency currency();

  public abstract InstrumentType type();

  /**
   * Serialize the current object and write to the provided output stream
   */
  @Override
  public void write(final LittleEndianDataOutputStream os) throws IOException {
    ByteStreamProcessor.appendString(os, symbol(), Instrument.MAX_SYMBOL_LENGTH + 1);
    os.writeInt(market().getValue());
    os.writeInt(currency().getValue());
    os.writeInt(type().getValue());
  }
}
