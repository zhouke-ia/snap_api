package com.integeralpha.api.snap.type;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

@SuppressWarnings("ClassWithTooManyMethods")
// This is a value class that's derived from the underlying SNAP C++ API
@AutoValue
public abstract class ExecReport {

  public static TypeAdapter<ExecReport> typeAdapter(final Gson gson) {
    return new AutoValue_ExecReport.GsonTypeAdapter(gson);
  }

  public abstract Instrument instrument();

  public abstract Side side();

  public abstract SellType sellType();

  public abstract double price();

  public abstract long qty();

  public abstract long filledQty();

  public abstract long openQty();

  public abstract double lastPrice();

  public abstract long lastQty();

  public abstract OrderType orderType();

  public abstract TimeInForce timeInForce();

  public abstract ExecType execType();

  public abstract OrderState orderState();

  public abstract long orderSeq();

  public abstract long baseId();

  public abstract String streetOrderId();

  public abstract String text();

  public abstract long instrumentId();

  public abstract long time();

  public abstract long traderId();

  public abstract long echoInt();

}
