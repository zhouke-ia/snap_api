package com.integeralpha.api.snap.type;

import com.google.auto.value.AutoValue;
import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

@SuppressWarnings({"StaticMethodOnlyUsedInOneClass", "ClassWithTooManyMethods"})
@AutoValue
public abstract class Order implements Transmittable {

  Order() {}

  public static Builder builder() {
    return new $AutoValue_Order.Builder();
  }

  // Calling builder
  public static Order read(final LittleEndianDataInputStream is) {
    // TODO
    return Order.builder().build();
  }

  public static TypeAdapter<Order> typeAdapter(final Gson gson) {
    return new AutoValue_Order.GsonTypeAdapter(gson);
  }

  public abstract Instrument instrument();

  public abstract Side side();

  public abstract SellType sellType();

  public abstract double price();

  public abstract long qty();

  public abstract long filledQty();

  public abstract long openQty();

  public abstract OrderType orderType();

  public abstract TimeInForce timeInForce();

  public abstract OrderState orderState();

  public abstract long orderSeq();

  public abstract long baseId();

  public abstract String streetOrderId();

  public abstract long instrumentID();

  public abstract long entryTime();

  public abstract long lastTime();

  public abstract long traderId();

  public abstract long senderId();

  public abstract long echoInt();

  /**
   * Serialize the current object and write to the provided output stream
   */
  @Override
  public void write(final LittleEndianDataOutputStream os) {
    //TODO
  }

  @AutoValue.Builder
  abstract static class Builder {

    public abstract Builder setInstrument(Instrument instrument);

    public abstract Builder setSide(Side side);

    public abstract Builder setSellType(SellType sellType);

    public abstract Builder setPrice(double price);

    public abstract Builder setQty(long qty);

    public abstract Builder setFilledQty(long filledQty);

    public abstract Builder setOpenQty(long openQty);

    public abstract Builder setOrderType(OrderType orderType);

    public abstract Builder setTimeInForce(TimeInForce tif);

    public abstract Builder setOrderState(OrderState orderState);

    public abstract Builder setOrderSeq(long orderSeq);

    public abstract Builder setBaseId(long baseId);

    public abstract Builder setStreetOrderId(String streetOrderId);

    public abstract Builder setInstrumentID(long instrumentID);

    public abstract Builder setEntryTime(long entryTime);

    public abstract Builder setLastTime(long lastTime);

    public abstract Builder setTraderId(long traderId);

    public abstract Builder setSenderId(long senderId);

    public abstract Builder setEchoInt(long echoInt);

    public abstract Order build();
  }


}
