package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum AlgoOrderState {
  PENDING(0),
  ACTIVE(1),
  TRIGGERED(2),
  REJECTED(3),
  PENDING_CXL(4),
  PENDING_RPL(5),
  CXL(6),
  RPL(7),
  CXL_REJ(8),
  RPL_REJ(9);

  private static final Map<Integer, AlgoOrderState> lookupMap
      = Maps.newHashMapWithExpectedSize(AlgoOrderState.values().length);

  static {
    for (final AlgoOrderState type : AlgoOrderState.values()) {
      AlgoOrderState.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  AlgoOrderState(final int value) {
    this.value = value;
  }

  public static AlgoOrderState fromValue(final int value) {
    return AlgoOrderState.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
