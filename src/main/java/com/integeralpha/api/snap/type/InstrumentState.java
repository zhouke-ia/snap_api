package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum InstrumentState {
  PRE_NR(1),
  TRADING_HALT(2),
  SUSPEND(3),
  ADJUST(4),
  PRE_OPEN(5),
  ENQUIRE(6),
  ABB_ACTION(7),
  ADJUST_ON(8),
  CLOSE(9),
  CSPA(10),
  LATE_TRADING(11),
  OPEN(12),
  OPEN_NIGHT_TRADING(13),
  PRE_CSPA(14),
  PRE_NIGHT_TRADING(15),
  PURGE_ORDERS(16),
  SYSTEM_MAINTENANCE(17),
  OPEN_QUOTE_DISPLAY(18),
  OPEN_VMB(19),
  WAIT_VMB(20);

  private static final Map<Integer, InstrumentState> lookupMap
      = Maps.newHashMapWithExpectedSize(InstrumentState.values().length);

  static {
    for (final InstrumentState type : InstrumentState.values()) {
      InstrumentState.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  InstrumentState(final int value) {
    this.value = value;
  }

  public static InstrumentState fromValue(final int value) {
    return InstrumentState.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }

}
