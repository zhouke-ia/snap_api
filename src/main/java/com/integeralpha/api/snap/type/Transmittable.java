package com.integeralpha.api.snap.type;

import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

/**
 * All classes implementing Transimittable interface shall provide a <b><i>public static
 * Trasmittable read(LittleEndianDataInputStream is) throws IOException </i></b> as well.
 */
@FunctionalInterface
public interface Transmittable  {

  /**
   * Serialize the current object and write to the provided output stream
   */
  void write(LittleEndianDataOutputStream os) throws IOException;
}
