package com.integeralpha.api.snap.type;

import com.google.common.collect.Maps;
import java.util.Map;

public enum Market {
  ASX(0),
  TSE(1),
  HKFE(2),
  TFX(3),
  SMX(4),
  SHFE(5),
  CBOT(6),
  SGX(7),
  CME(8),
  CFE(9),
  FOR(10),
  SFE(11),
  ICE_IPE(12),
  OSE(13),
  EUREX(14),
  TOCOM(15),
  SET(16),
  LME(17),
  NYMEX(18),
  TFEX(19);

  private static final Map<Integer, Market> lookupMap
      = Maps.newHashMapWithExpectedSize(Market.values().length);

  static {
    for (final Market type : Market.values()) {
      Market.lookupMap.put(type.value, type);
    }
  }

  private final int value;

  Market(final int value) {
    this.value = value;
  }

  public static Market fromValue(final int value) {
    return Market.lookupMap.get(value);
  }

  public int getValue() {
    return value;
  }
}
