package com.integeralpha.api.snap.message.event;

import com.google.common.io.LittleEndianDataInputStream;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.PositionReport;
import java.io.IOException;

public class PositionEvent implements SnapIncomingMessage {

  private static final int MSG_LENGTH = 64;
  private static final MsgType MSG_TYPE = MsgType.REP_POSITION;
  private final PositionReport positionReport = null;

  private long msgTime;

  @Override
  public MsgType getMsgType() {
    return PositionEvent.MSG_TYPE;
  }

  @Override
  public long getMsgLength() {
    return PositionEvent.MSG_LENGTH;
  }

  @Override
  public void read(final LittleEndianDataInputStream is, final Header header) throws IOException {
    msgTime = header.getTime();
    //TODO
  }

  @Override
  public long getMsgTime() {
    return msgTime;
  }

  public PositionReport getPositionReport() {
    return positionReport;
  }
}
