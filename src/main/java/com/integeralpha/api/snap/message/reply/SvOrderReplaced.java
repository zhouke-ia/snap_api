package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.MsgType;

@AutoService(SnapIncomingMessage.class)
public final class SvOrderReplaced extends SvOrderReport {

  private SvOrderReplaced() {}
  private static final MsgType MSG_TYPE = MsgType.REP_SV_RPL;

  @Override
  public MsgType getMsgType() {
    return SvOrderReplaced.MSG_TYPE;
  }
}
