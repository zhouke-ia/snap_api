package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.type.MsgType;

public class Reject extends Execution {

  private static final MsgType MSG_TYPE = MsgType.REP_REJ;

  @Override
  public MsgType getMsgType() {
    return Reject.MSG_TYPE;
  }
}
