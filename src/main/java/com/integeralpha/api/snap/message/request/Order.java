package com.integeralpha.api.snap.message.request;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.Instrument;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.OrderType;
import com.integeralpha.api.snap.type.SellType;
import com.integeralpha.api.snap.type.Side;
import com.integeralpha.api.snap.type.TimeInForce;
import com.integeralpha.api.snap.type.Transmittable;
import java.io.IOException;

/**
 * To send a new order
 */
@SuppressWarnings("ClassWithTooManyFields")
// Wrapper of SNAP API
public final class Order extends AbstractSnapOutgoingMessage implements Transmittable {

  private static final int MSG_LENGTH = 168;
  private static final MsgType MSG_TYPE = MsgType.REQ_NEW_ORDER;
  private final Instrument instrument;
  private final Side side;
  private final SellType sellType;
  private final double price;
  private final long quantity;
  private final OrderType orderType;
  private final TimeInForce timeInForce;
  private final long sequenceNumber;
  private final long traderId;
  private final long echoInt;

  private Order(final Instrument instrument,
                final Side side,
                final SellType sellType,
                final double price,
                final long quantity,
                final OrderType orderType,
                final TimeInForce timeInForce,
                final long sequenceNumber,
                final long traderId,
                final long echoInt) {
    super(Order.MSG_TYPE, Order.MSG_LENGTH);
    this.instrument = instrument;
    this.side = side;
    this.sellType = sellType;
    this.price = price;
    this.quantity = quantity;
    this.orderType = orderType;
    this.timeInForce = timeInForce;
    this.sequenceNumber = sequenceNumber;
    this.traderId = traderId;
    this.echoInt = echoInt;
  }

  private Order(final Header header,
                final Instrument instrument,
                final Side side,
                final SellType sellType,
                final double price,
                final long quantity,
                final OrderType orderType,
                final TimeInForce timeInForce,
                final long sequenceNumber,
                final long traderId,
                final long echoInt) {
    super(header);
    this.instrument = instrument;
    this.side = side;
    this.sellType = sellType;
    this.price = price;
    this.quantity = quantity;
    this.orderType = orderType;
    this.timeInForce = timeInForce;
    this.sequenceNumber = sequenceNumber;
    this.traderId = traderId;
    this.echoInt = echoInt;
  }

  public static Builder builder() {
    return Builder.builder();
  }

  /**
   * New order requests are enclosed in SV reports. When read from input stream, it comes with its
   * own headers. Below method verifies the integrity of the raw input and rebuild the order object
   *
   * @param is the InputStream
   * @return the de-serialized order object
   * @throws IOException when an I/O error occurs, or the provided InputStream doesn't de-serialize
   * into an Order object
   */

  public static Order read(final LittleEndianDataInputStream is) throws IOException {
    final Header header = Header.read(is);
    final MsgType type = header.getType();
    if (type != Order.MSG_TYPE) {
      throw new IOException("MsgType " + type + " does not match expected " + Order.MSG_TYPE);
    }
    //TODO
    return Order.builder()
                .setInstrument(Instrument.read(is))
                .setSide(Side.fromValue(is.readInt()))
                .setPrice(is.readDouble())
                .setQuantity(is.readLong())
                .setOrderType(OrderType.fromValue(is.readInt()))
                .setTimeInForce(TimeInForce.fromValue(is.readInt()))
                .setSequenceNumber(is.readLong())
                .setTraderId(is.readInt())
                .setEchoInt(is.readLong())
                .build();
  }

  @Override
  public void write(final LittleEndianDataOutputStream os) throws IOException {
    header.write(os);
    instrument.write(os);
    os.writeInt(side.getValue());
    os.writeLong(sellType.getValue());
    os.writeDouble(price);
    os.writeLong(quantity);
    os.writeInt(orderType.getValue());
    os.writeInt(timeInForce.getValue());
    os.writeLong(sequenceNumber);
    os.writeLong(traderId);
    os.writeLong(echoInt);
  }

  @SuppressWarnings({"PublicInnerClass", "WeakerAccess"})
  // builder
  public static final class Builder {

    private Header header = null;
    private Instrument instrument = null;
    private Side side = null;
    private SellType sellType = SellType.NONE;
    private double price;
    private long quantity;
    private OrderType orderType = null;
    private TimeInForce timeInForce = TimeInForce.DAY;
    private long sequenceNumber;
    private long traderId;
    private long echoInt;

    private Builder() {}

    public static Builder builder() {
      return new Builder();
    }

    public Builder setInstrument(final Instrument instrument) {
      this.instrument = instrument;
      return this;
    }

    public Builder setHeader(final Header header) {
      this.header = header;
      return this;
    }

    public Builder setSide(final Side side) {
      this.side = side;
      return this;
    }

    public Builder setSellType(final SellType sellType) {
      this.sellType = sellType;
      return this;
    }

    public Builder setPrice(final double price) {
      this.price = price;
      return this;
    }

    public Builder setQuantity(final long quantity) {
      this.quantity = quantity;
      return this;
    }

    public Builder setOrderType(final OrderType orderType) {
      this.orderType = orderType;
      return this;
    }

    public Builder setTimeInForce(final TimeInForce timeInForce) {
      this.timeInForce = timeInForce;
      return this;
    }

    public Builder setSequenceNumber(final long sequenceNumber) {
      this.sequenceNumber = sequenceNumber;
      return this;
    }

    public Builder setTraderId(final long traderId) {
      this.traderId = traderId;
      return this;
    }

    public Builder setEchoInt(final long echoInt) {
      this.echoInt = echoInt;
      return this;
    }

    public Order build() {
      return (header == null) ?
             new Order(instrument, side, sellType, price, quantity, orderType, timeInForce,
                       sequenceNumber, traderId, echoInt) :
             new Order(header, instrument, side, sellType, price, quantity, orderType,
                       timeInForce, sequenceNumber, traderId, echoInt);
    }
  }

  @Override
  public String toString() {
    return "Order{" +
           "instrument=" + instrument +
           ", side=" + side +
           ", sellType=" + sellType +
           ", price=" + price +
           ", quantity=" + quantity +
           ", orderType=" + orderType +
           ", timeInForce=" + timeInForce +
           ", sequenceNumber=" + sequenceNumber +
           ", traderId=" + traderId +
           ", echoInt=" + echoInt +
           ", header=" + header +
           '}';
  }
}
