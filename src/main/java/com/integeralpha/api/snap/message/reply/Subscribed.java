package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.google.common.io.LittleEndianDataInputStream;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.Instrument;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.Option;
import com.integeralpha.api.snap.type.SubscriptionType;
import com.integeralpha.api.snap.util.ByteStreamProcessor;
import java.io.IOException;

@SuppressWarnings("ClassWithTooManyFields")
// From SNAP API
@AutoService(SnapIncomingMessage.class)
public final class Subscribed implements SnapIncomingMessage {

  private static final int MSG_LENGTH = 168;
  private static final MsgType MSG_TYPE = MsgType.REP_SUBSCRIBE;

  private long msgTime;
  private Instrument instrument = null;
  private SubscriptionType subscriptionType = null;
  private String expiryDate = null;
  private String expiryDateDayOfWeek = null;
  private long strike_Price;
  private Option option = null;
  private long instrumentId;
  private long decimalValue;
  private String tickSize = null;
  private String product = null;

  private Subscribed() {}

  @Override
  public long getMsgLength() {
    return Subscribed.MSG_LENGTH;
  }

  @SuppressWarnings({"NumericCastThatLosesPrecision", "FeatureEnvy"})
  // Based on SNAP design, uses utility class
  @Override
  public void read(final LittleEndianDataInputStream is, final Header header)
      throws IOException {
    msgTime = header.getTime();
    instrument = Instrument.read(is);
    subscriptionType = SubscriptionType.fromValue(is.readInt());
    expiryDate = ByteStreamProcessor.readString(is, 6);
    expiryDateDayOfWeek = ByteStreamProcessor.readString(is, 2);
    strike_Price = is.readLong();
    option = Option.fromValue((int) is.readLong());
    instrumentId = is.readLong();
    decimalValue = is.readLong();
    tickSize = ByteStreamProcessor.readString(is, 8);
    product = ByteStreamProcessor.readString(is, 8);
  }

  @Override
  public long getMsgTime() {
    return msgTime;
  }

  @Override
  public MsgType getMsgType() {
    return Subscribed.MSG_TYPE;
  }

  public Instrument getInstrument() {
    return instrument;
  }

  public SubscriptionType getSubscriptionType() {
    return subscriptionType;
  }

  public String getExpiryDate() {
    return expiryDate;
  }

  public String getExpiryDateDayOfWeek() {
    return expiryDateDayOfWeek;
  }

  public long getStrike_Price() {
    return strike_Price;
  }

  public Option getOption() {
    return option;
  }

  public long getInstrumentId() {
    return instrumentId;
  }

  public long getDecimalValue() {
    return decimalValue;
  }

  public String getTickSize() {
    return tickSize;
  }

  public String getProduct() {
    return product;
  }

  @Override
  public String toString() {
    return "Subscribed{" +
           "msgTime=" + msgTime +
           ", instrument=" + instrument +
           ", subscriptionType=" + subscriptionType +
           ", expiryDate='" + expiryDate + '\'' +
           ", expiryDateDayOfWeek='" + expiryDateDayOfWeek + '\'' +
           ", strike_Price=" + strike_Price +
           ", option=" + option +
           ", instrumentId=" + instrumentId +
           ", decimalValue=" + decimalValue +
           ", tickSize='" + tickSize + '\'' +
           ", product='" + product + '\'' +
           ", msgLength=" + getMsgLength() +
           ", msgType=" + Subscribed.MSG_TYPE +
           '}';
  }
}
