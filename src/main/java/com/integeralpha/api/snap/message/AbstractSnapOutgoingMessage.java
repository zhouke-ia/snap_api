package com.integeralpha.api.snap.message;

import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.MsgType;

public abstract class AbstractSnapOutgoingMessage implements SnapOutgoingMessage {

  protected final Header header;

  protected AbstractSnapOutgoingMessage(final MsgType msgType, final int msgLength) {
    header = Header.create(msgType, msgLength);
  }

  protected AbstractSnapOutgoingMessage(final Header header) {
    this.header = header;
  }
}
