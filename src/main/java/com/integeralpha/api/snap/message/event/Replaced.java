package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.type.MsgType;

public class Replaced extends Execution {

  private static final MsgType MSG_TYPE = MsgType.REP_RPL;

  @Override
  public MsgType getMsgType() {
    return Replaced.MSG_TYPE;
  }
}
