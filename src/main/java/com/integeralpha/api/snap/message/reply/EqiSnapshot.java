package com.integeralpha.api.snap.message.reply;

import com.integeralpha.api.snap.type.MsgType;
import java.util.Arrays;

public final class EqiSnapshot extends MarketDataSnapshot {

  private static final int MSG_LENGTH = 168;
  private static final MsgType MSG_TYPE = MsgType.REP_EQI_SNAPSHOT;

  private EqiSnapshot() {}

  @Override
  public MsgType getMsgType() {
    return EqiSnapshot.MSG_TYPE;
  }

  @Override
  public long getMsgLength() {
    return EqiSnapshot.MSG_LENGTH;
  }

  @Override
  public String toString() {
    return "EqiSnapshot{" +
           "msgType=" + EqiSnapshot.MSG_TYPE +
           ", msgLength=" + getMsgLength() +
           ", msgTime=" + getMsgTime() +
           ", instrumentId=" + getInstrumentId() +
           ", items=" + Arrays.toString(getItems()) +
           '}';
  }
}
