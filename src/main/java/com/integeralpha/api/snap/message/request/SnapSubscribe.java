package com.integeralpha.api.snap.message.request;

import com.google.common.io.LittleEndianDataOutputStream;
import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.Instrument;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.SubscriptionType;
import java.io.IOException;

public final class SnapSubscribe extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 120;
  private static final MsgType MSG_TYPE = MsgType.REQ_SUBSCRIBE;
  private final Instrument instrument;
  private final SubscriptionType subscriptionType;
  private final boolean unsubscribe;

  private SnapSubscribe(final Instrument instrument,
                        final SubscriptionType subscriptionType, final Unsub unsub) {
    super(SnapSubscribe.MSG_TYPE, SnapSubscribe.MSG_LENGTH);
    this.instrument = instrument;
    this.subscriptionType = subscriptionType;
    unsubscribe = (unsub == Unsub.TRUE);
  }

  public static SnapSubscribe create(final Instrument instrument,
                                     final SubscriptionType subscriptionType, final Unsub unsub) {
    return new SnapSubscribe(instrument, subscriptionType, unsub);
  }

  /**
   * Serialize the current object and write to the provided output stream
   */
  @Override
  public void write(final LittleEndianDataOutputStream os) throws IOException {
    header.write(os);
    instrument.write(os);
    os.writeInt(subscriptionType.getValue());
    os.writeLong(unsubscribe ? 1 : 0);
  }

  public enum Unsub {
    TRUE, FALSE
  }

  @Override
  public String toString() {
    return "SnapSubscribe{" +
           "instrument=" + instrument +
           ", subscriptionType=" + subscriptionType +
           ", unsubscribe=" + unsubscribe +
           ", header=" + header +
           '}';
  }
}
