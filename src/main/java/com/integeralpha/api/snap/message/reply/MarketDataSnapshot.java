package com.integeralpha.api.snap.message.reply;

import com.google.common.io.LittleEndianDataInputStream;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.MarketDataItem;
import java.io.IOException;

public abstract class MarketDataSnapshot implements SnapIncomingMessage {

  private long msgTime;
  private long instrumentId;
  private MarketDataItem[] items = null;

  @Override
  public long getMsgTime() {
    return msgTime;
  }

  public void setMsgTime(final long msgTime) {
    this.msgTime = msgTime;
  }

  public long getInstrumentId() {
    return instrumentId;
  }

  public void setInstrumentId(final long instrumentId) {
    this.instrumentId = instrumentId;
  }

  public MarketDataItem[] getItems() {
    return items.clone();
  }

  public void setItems(final MarketDataItem[] items) {
    this.items = items.clone();
  }

  @Override
  public void read(final LittleEndianDataInputStream is, final Header header) throws IOException {
    this.msgTime = header.getTime();
    //TODO
  }
}
