package com.integeralpha.api.snap.message.event;

import com.google.common.io.LittleEndianDataInputStream;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.ExecReport;
import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.Order;
import java.io.IOException;

public abstract class Execution implements SnapIncomingMessage {

  private static final int MSG_LENGTH = 648;

  private final Order originalOrder = null;
  private final ExecReport execReport = null;
  private long msgTime;

  @Override
  public final void read(final LittleEndianDataInputStream is, final Header header) throws IOException {
    msgTime = header.getTime();
    //TODO
  }

  @Override
  public final long getMsgTime() {
    return msgTime;
  }

  @Override
  public final long getMsgLength() {
    return Execution.MSG_LENGTH;
  }

  public final ExecReport getExecReport() {
    return execReport;
  }

}
