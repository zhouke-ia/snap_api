package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.google.common.io.LittleEndianDataInputStream;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.MsgType;
import java.io.IOException;

@AutoService(SnapIncomingMessage.class)
public final class PositionList implements SnapIncomingMessage {

  private static final int MSG_LENGTH = 48;
  private static final MsgType MSG_TYPE = MsgType.REP_POS_LIST;

  private long msgTime;
  private long requestId;
  private long numberOfPositions;

  private PositionList() {}

  @Override
  public MsgType getMsgType() {
    return PositionList.MSG_TYPE;
  }

  @Override
  public long getMsgLength() {
    return PositionList.MSG_LENGTH;
  }

  @Override
  public void read(final LittleEndianDataInputStream is, final Header header) throws IOException {
    msgTime = header.getTime();
    //TODO
  }

  @Override
  public long getMsgTime() {
    return msgTime;
  }

  @Override
  public String toString() {
    return "PositionList{" +
           "msgTime=" + msgTime +
           ", requestId=" + requestId +
           ", numberOfPositions=" + numberOfPositions +
           ", msgType=" + PositionList.MSG_TYPE +
           ", msgLength=" + getMsgLength() +
           '}';
  }

  public long getRequestId() {
    return requestId;
  }

  public long getNumberOfPositions() {
    return numberOfPositions;
  }
}
