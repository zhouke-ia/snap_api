package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.type.MsgType;

public class Fill extends Execution {

  private static final MsgType MSG_TYPE = MsgType.REP_FILL;

  @Override
  public MsgType getMsgType() {
    return Fill.MSG_TYPE;
  }
}
