package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.MsgType;
import java.util.Arrays;

@AutoService(SnapIncomingMessage.class)
public final class TradeSnapshot extends MarketDataSnapshot {

  public static final int MSG_LENGTH = 104;
  private static final MsgType MSG_TYPE = MsgType.REP_TRD_SNAPSHOT;

  private TradeSnapshot() {}

  @Override
  public MsgType getMsgType() {
    return TradeSnapshot.MSG_TYPE;
  }

  @Override
  public long getMsgLength() {
    return TradeSnapshot.MSG_LENGTH;
  }

  @Override
  public String toString() {
    return "TradeSnapshot{" +
           "msgType=" + TradeSnapshot.MSG_TYPE +
           ", msgLength=" + getMsgLength() +
           ", msgTime=" + getMsgTime() +
           ", instrumentId=" + getInstrumentId() +
           ", items=" + Arrays.toString(getItems()) +
           '}';
  }
}
