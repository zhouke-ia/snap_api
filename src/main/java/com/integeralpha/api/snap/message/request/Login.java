package com.integeralpha.api.snap.message.request;

import com.google.common.io.LittleEndianDataOutputStream;
import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.message.SnapOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.util.ByteStreamProcessor;
import java.io.IOException;

/**
 * Login Message
 */
public final class Login extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 48;
  private static final MsgType MSG_TYPE = MsgType.REQ_LOGIN;

  private static final int BUFFER_SIZE = 8;

  private final String loginId;
  private final String password;

  private Login(final String loginId, final String password) {
    super(Login.MSG_TYPE, Login.MSG_LENGTH);
    this.loginId = loginId;
    this.password = password;
  }

  public static Login create(final String loginId, final String password) {
    if ((loginId.length() > (Login.BUFFER_SIZE - 1)) ||
        (password.length() > (Login.BUFFER_SIZE - 1))) {
      throw new IllegalArgumentException("Maximum length allowed is " + (Login.BUFFER_SIZE - 1));
    }
    return new Login(loginId, password);
  }

  /**
   * @see SnapOutgoingMessage
   */
  @Override
  public void write(final LittleEndianDataOutputStream os) throws IOException {
    header.write(os);
    ByteStreamProcessor.appendString(os, loginId, Login.BUFFER_SIZE);
    ByteStreamProcessor.appendString(os, password, Login.BUFFER_SIZE);
  }

  @Override
  public String toString() {
    return "Login{" +
           "loginId='" + loginId + '\'' +
           ", password='" + password + '\'' +
           ", header=" + header +
           '}';
  }
}
