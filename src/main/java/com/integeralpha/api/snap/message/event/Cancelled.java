package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.type.MsgType;

public class Cancelled extends Execution {

  private static final MsgType MSG_TYPE = MsgType.REP_CXL;

  @Override
  public MsgType getMsgType() {
    return Cancelled.MSG_TYPE;
  }
}
