package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.type.MsgType;

public class Partial extends Execution {

  private static final MsgType MSG_TYPE = MsgType.REP_PART;

  @Override
  public MsgType getMsgType() {
    return Partial.MSG_TYPE;
  }
}
