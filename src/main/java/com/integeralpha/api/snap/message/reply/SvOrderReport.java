package com.integeralpha.api.snap.message.reply;

import com.google.common.io.LittleEndianDataInputStream;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.message.request.Order;
import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.Instrument;
import com.integeralpha.api.snap.type.Side;
import java.io.IOException;


public abstract class SvOrderReport implements SnapIncomingMessage {

  //TODO

  private static final int MSG_LENGTH = 320;

  private long msgTime;
  private Instrument triggerInstrument = null;
  private Side triggerSide = null;
  private double triggerPrice;
  private long triggerQuantity;
  private long svOrderSequence;
  private Order order = null;
  private long instrumentId;
  private long triggerInstrumentId;

  @Override
  public final long getMsgTime() {
    return msgTime;
  }

  public final Instrument getTriggerInstrument() {
    return triggerInstrument;
  }

  public final Side getTriggerSide() {
    return triggerSide;
  }

  public final double getTriggerPrice() {
    return triggerPrice;
  }

  public final long getTriggerQuantity() {
    return triggerQuantity;
  }

  public final long getSvOrderSequence() {
    return svOrderSequence;
  }

  public final Order getOrder() {
    return order;
  }

  public final long getInstrumentId() {
    return instrumentId;
  }

  public final long getTriggerInstrumentId() {
    return triggerInstrumentId;
  }

  public final void read(final LittleEndianDataInputStream is, final Header header)
      throws IOException {
    msgTime = header.getTime();
    triggerInstrument = Instrument.read(is);
    triggerSide = Side.fromValue(is.readInt());
    triggerPrice = is.readDouble();
    triggerQuantity = is.readLong();
    svOrderSequence = is.readLong();
    order = Order.read(is);
    instrumentId = is.readLong();
    triggerInstrumentId = is.readLong();
  }

  @Override
  public final long getMsgLength() {
    return SvOrderReport.MSG_LENGTH;
  }

  @Override
  public final String toString() {
    return "SvOrderReport{" +
           "msgType=" + getMsgType() +
           ", msgTime=" + msgTime +
           ", triggerInstrument=" + triggerInstrument +
           ", triggerSide=" + triggerSide +
           ", triggerPrice=" + triggerPrice +
           ", triggerQuantity=" + triggerQuantity +
           ", svOrderSequence=" + svOrderSequence +
           ", order=" + order +
           ", instrumentId=" + instrumentId +
           ", triggerInstrumentId=" + triggerInstrumentId +
           '}';
  }
}
