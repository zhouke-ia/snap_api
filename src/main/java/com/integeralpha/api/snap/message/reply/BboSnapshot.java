package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.MsgType;
import java.util.Arrays;

@AutoService(SnapIncomingMessage.class)
public final class BboSnapshot extends MarketDataSnapshot {

  public static final int MSG_LENGTH = 168;
  private static final MsgType MSG_TYPE = MsgType.REP_BBO_SNAPSHOT;

  private BboSnapshot() {}

  @Override
  public MsgType getMsgType() {
    return BboSnapshot.MSG_TYPE;
  }

  @Override
  public long getMsgLength() {
    return BboSnapshot.MSG_LENGTH;
  }

  @Override
  public String toString() {
    return "BboSnapshot{" +
           "msgType=" + BboSnapshot.MSG_TYPE +
           ", msgLength=" + getMsgLength() +
           ", msgTime=" + getMsgTime() +
           ", instrumentId=" + getInstrumentId() +
           ", items=" + Arrays.toString(getItems()) +
           '}';
  }
}
