package com.integeralpha.api.snap.message;

import com.google.common.io.LittleEndianDataOutputStream;
import java.io.IOException;

@FunctionalInterface
public interface SnapOutgoingMessage {

  /**
   * Serialize the current object and write to the provided output stream
   */
  void write(LittleEndianDataOutputStream os) throws IOException;
}
