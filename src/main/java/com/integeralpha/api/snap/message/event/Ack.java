package com.integeralpha.api.snap.message.event;

import com.integeralpha.api.snap.type.MsgType;

public class Ack extends Execution {

  private static final MsgType MSG_TYPE = MsgType.REP_ACK;

  @Override
  public MsgType getMsgType() {
    return Ack.MSG_TYPE;
  }
}
