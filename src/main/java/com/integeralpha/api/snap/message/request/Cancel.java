package com.integeralpha.api.snap.message.request;

import com.google.common.io.LittleEndianDataOutputStream;
import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;
import java.io.IOException;

/**
 * To cancel an existing order
 */
public final class Cancel extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 48;
  private static final MsgType MSG_TYPE = MsgType.REQ_CXL;
  private final long sequenceNumber;
  private final long originalSequenceNumber;

  private Cancel(final long sequenceNumber, final long origOrderId) {
    super(Cancel.MSG_TYPE, Cancel.MSG_LENGTH);
    this.sequenceNumber = sequenceNumber;
    originalSequenceNumber = origOrderId;
  }

  public static Cancel create(final long sequenceNumber, final long origOrderId) {
    return new Cancel(sequenceNumber, origOrderId);
  }

  /**
   * Serialize the current object and write to the provided output stream
   */
  @Override
  public void write(final LittleEndianDataOutputStream os) throws IOException {
    header.write(os);
    os.writeLong(sequenceNumber);
    os.writeLong(originalSequenceNumber);
  }

  @Override
  public String toString() {
    return "Cancel{" +
           "sequenceNumber=" + sequenceNumber +
           ", originalSequenceNumber=" + originalSequenceNumber +
           ", header=" + header +
           '}';
  }
}
