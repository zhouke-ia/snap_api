package com.integeralpha.api.snap.message.request;

import com.google.common.io.LittleEndianDataOutputStream;
import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.MsgType;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

public final class ListRequest extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 40;

  private static final AtomicLong requestId = new AtomicLong();

  private ListRequest(final MsgType msgType) {
    super(msgType, ListRequest.MSG_LENGTH);
  }

  public static ListRequest create(final ListType type) {
    switch (type) {
      case ORDER:
        return new ListRequest(MsgType.REQ_ORDERS);
      case SV_ORDER:
        return new ListRequest(MsgType.REQ_SV_LIST);
      case POSITION:
        return new ListRequest(MsgType.REQ_POS);
      case TRADE:
        return new ListRequest(MsgType.REQ_TRADES);
    }
    throw new IllegalArgumentException("Invalid type " + type);
  }

  @Override
  public void write(final LittleEndianDataOutputStream os) throws IOException {
    header.write(os);
    os.writeLong(ListRequest.requestId.incrementAndGet());
  }

  public enum ListType {
    ORDER, SV_ORDER, POSITION, TRADE
  }
}
