package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.MsgType;

@AutoService(SnapIncomingMessage.class)
public final class SvOrderCxlRejected extends SvOrderReport {

  private static final MsgType MSG_TYPE = MsgType.REP_SV_CXL_REJ;

  private SvOrderCxlRejected() {}

  @Override
  public MsgType getMsgType() {
    return SvOrderCxlRejected.MSG_TYPE;
  }
}
