package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.MsgType;
import java.util.Arrays;

@AutoService(SnapIncomingMessage.class)
public final class StaSnapshot extends MarketDataSnapshot {

  public static final int MSG_LENGTH = 168;
  private static final MsgType MSG_TYPE = MsgType.REP_STA_SNAPSHOT;

  private StaSnapshot() {}

  @Override
  public MsgType getMsgType() {
    return StaSnapshot.MSG_TYPE;
  }

  @Override
  public long getMsgLength() {
    return StaSnapshot.MSG_LENGTH;
  }

  @Override
  public String toString() {
    return "StaSnapshot{" +
           "msgType=" + StaSnapshot.MSG_TYPE +
           ", msgLength=" + getMsgLength() +
           ", msgTime=" + getMsgTime() +
           ", instrumentId=" + getInstrumentId() +
           ", items=" + Arrays.toString(getItems()) +
           '}';
  }
}
