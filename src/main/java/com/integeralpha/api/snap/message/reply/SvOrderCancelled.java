package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.MsgType;

@AutoService(SnapIncomingMessage.class)
public final class SvOrderCancelled extends SvOrderReport {

  private static final MsgType MSG_TYPE = MsgType.REP_SV_CXL;

  private SvOrderCancelled() {}

  @Override
  public MsgType getMsgType() {
    return SvOrderCancelled.MSG_TYPE;
  }
}
