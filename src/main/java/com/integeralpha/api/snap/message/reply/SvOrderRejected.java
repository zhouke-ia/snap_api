package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.MsgType;

@AutoService(SnapIncomingMessage.class)
public final class SvOrderRejected extends SvOrderReport {

  private static final MsgType MSG_TYPE = MsgType.REP_SV_REJ;

  private SvOrderRejected() {}

  @Override
  public MsgType getMsgType() {
    return SvOrderRejected.MSG_TYPE;
  }
}
