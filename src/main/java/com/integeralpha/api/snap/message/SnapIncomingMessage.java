package com.integeralpha.api.snap.message;

import com.google.common.io.LittleEndianDataInputStream;
import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.MsgType;
import java.io.IOException;
import java.util.ServiceLoader;

@SuppressWarnings("StaticMethodOnlyUsedInOneClass")
//Interface class
public interface SnapIncomingMessage {

  static SnapIncomingMessage read(final LittleEndianDataInputStream is) throws IOException {
    final Header header = Header.read(is);
    final ServiceLoader<SnapIncomingMessage> loader = ServiceLoader.load(SnapIncomingMessage.class);
    final MsgType type = header.getType();
    for (final SnapIncomingMessage msg : loader) {
      if (msg.getMsgType() == type) {
        msg.read(is, header);
        return msg;
      }
    }
    throw new IllegalArgumentException(
        type.name() + " is not a valid type of incoming message.");
  }

  MsgType getMsgType();

  long getMsgLength();

  void read(LittleEndianDataInputStream is, Header header) throws IOException;

  long getMsgTime();
}
