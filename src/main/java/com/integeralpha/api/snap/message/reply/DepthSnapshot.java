package com.integeralpha.api.snap.message.reply;

import com.google.auto.service.AutoService;
import com.integeralpha.api.snap.message.SnapIncomingMessage;
import com.integeralpha.api.snap.type.MsgType;
import java.util.Arrays;

@AutoService(SnapIncomingMessage.class)
public final class DepthSnapshot extends MarketDataSnapshot {

  public static final int MSG_LENGTH = 680;
  private static final MsgType MSG_TYPE = MsgType.REP_DEPTH_SNAPSHOT;

  private DepthSnapshot() {}

  @Override
  public MsgType getMsgType() {
    return DepthSnapshot.MSG_TYPE;
  }

  @Override
  public long getMsgLength() {
    return DepthSnapshot.MSG_LENGTH;
  }

  @Override
  public String toString() {
    return "DepthSnapshot{" +
           "msgType=" + DepthSnapshot.MSG_TYPE +
           ", msgLength=" + getMsgLength() +
           ", msgTime=" + getMsgTime() +
           ", instrumentId=" + getInstrumentId() +
           ", items=" + Arrays.toString(getItems()) +
           '}';
  }
}
