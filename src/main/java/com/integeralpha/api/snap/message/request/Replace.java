package com.integeralpha.api.snap.message.request;

import com.google.common.io.LittleEndianDataOutputStream;
import com.integeralpha.api.snap.message.AbstractSnapOutgoingMessage;
import com.integeralpha.api.snap.type.Header;
import com.integeralpha.api.snap.type.Instrument;
import com.integeralpha.api.snap.type.MsgType;
import com.integeralpha.api.snap.type.OrderType;
import com.integeralpha.api.snap.type.SellType;
import com.integeralpha.api.snap.type.Side;
import com.integeralpha.api.snap.type.TimeInForce;

/**
 * To replace an order
 */
@SuppressWarnings("ClassWithTooManyFields")
// Wrapping SNAP C++ API
public final class Replace extends AbstractSnapOutgoingMessage {

  private static final int MSG_LENGTH = 160;
  private static final MsgType MSG_TYPE = MsgType.REQ_RPL;
  private final Instrument instrument;
  private final Side side;
  private final SellType sellType;
  private final double price;
  private final long quantity;
  private final OrderType orderType;
  private final TimeInForce timeInForce;
  private final long sequenceNumber;
  private final long traderId;
  private final long echoInt;
  private final long originalOrderSeq;

  private Replace(final Instrument instrument, final Side side, final SellType sellType, final double price, final long quantity,
                  final OrderType orderType, final TimeInForce timeInForce, final long sequenceNumber, final long traderId,
                  final long echoInt, final long originalOrderSeq) {
    super(Replace.MSG_TYPE, Replace.MSG_LENGTH);
    this.instrument = instrument;
    this.side = side;
    this.sellType = sellType;
    this.price = price;
    this.quantity = quantity;
    this.orderType = orderType;
    this.timeInForce = timeInForce;
    this.sequenceNumber = sequenceNumber;
    this.traderId = traderId;
    this.echoInt = echoInt;
    this.originalOrderSeq = originalOrderSeq;
  }

  public static Builder builder() {
    return new Builder();
  }

  /**
   * Serialize the current object and write to the provided output stream
   */
  @Override
  public void write(final LittleEndianDataOutputStream os) {
    //TODO
  }

  @SuppressWarnings("PublicInnerClass")
  // builder
  public static final class Builder {

    Builder() {}

    private Header header = null;
    private Instrument instrument = null;
    private Side side = null;
    private SellType sellType = SellType.NONE;
    private double price;
    private long quantity;
    private OrderType orderType = null;
    private TimeInForce timeInForce = TimeInForce.DAY;
    private long sequenceNumber;
    private long traderId;
    private long echoInt;
    private long originalOrderSeq;

    public Builder setOriginalOrderSeq(final long originalOrderSeq) {
      this.originalOrderSeq = originalOrderSeq;
      return this;
    }

    public Builder setInstrument(final Instrument instrument) {
      this.instrument = instrument;
      return this;
    }

    public Builder setHeader(final Header header) {
      this.header = header;
      return this;
    }

    public Builder setSide(final Side side) {
      this.side = side;
      return this;
    }

    public Builder setSellType(final SellType sellType) {
      this.sellType = sellType;
      return this;
    }

    public Builder setPrice(final double price) {
      this.price = price;
      return this;
    }

    public Builder setQuantity(final long quantity) {
      this.quantity = quantity;
      return this;
    }

    public Builder setOrderType(final OrderType orderType) {
      this.orderType = orderType;
      return this;
    }

    public Builder setTimeInForce(final TimeInForce timeInForce) {
      this.timeInForce = timeInForce;
      return this;
    }

    public Builder setSequenceNumber(final long sequenceNumber) {
      this.sequenceNumber = sequenceNumber;
      return this;
    }

    public Builder setTraderId(final long traderId) {
      this.traderId = traderId;
      return this;
    }

    public Builder setEchoInt(final long echoInt) {
      this.echoInt = echoInt;
      return this;
    }

    public Replace build() {
      return new Replace(instrument, side, sellType, price, quantity, orderType, timeInForce,
          sequenceNumber, traderId, echoInt, originalOrderSeq); //TODO
    }
  }

  @Override
  public String toString() {
    return "Replace{" +
           "instrument=" + instrument +
           ", side=" + side +
           ", sellType=" + sellType +
           ", price=" + price +
           ", quantity=" + quantity +
           ", orderType=" + orderType +
           ", timeInForce=" + timeInForce +
           ", sequenceNumber=" + sequenceNumber +
           ", echoInt=" + echoInt +
           ", originalOrderSeq=" + originalOrderSeq +
           ", header=" + header +
           '}';
  }
}
