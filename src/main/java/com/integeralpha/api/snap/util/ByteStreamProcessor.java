package com.integeralpha.api.snap.util;

import com.google.common.primitives.Bytes;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;


/**
 * This class contains static methods that are used in this project for byte level operations.
 */
@SuppressWarnings("StaticMethodOnlyUsedInOneClass")
// Util class
public enum ByteStreamProcessor {
  ;

  /**
   * Convert a string into a fixed length ANSI encoded byte array and write to given OutputStream.
   *
   * @param os the OutputStream
   * @param input String to write, may contain only ANSI characters
   * @param length length in bytes to write in the output stream
   * @throws IOException when an I/O error occurs
   */
  public static void appendString(final OutputStream os, final String input, final int length)
      throws IOException {
    final byte[] array = new byte[length];
    System.arraycopy(
        input.getBytes(Charset.forName("Cp1252")), 0, array, 0, input.length());
    os.write(array);
  }

  /**
   * Read a Java string from given InputStream, and consume the fixed number of bytes specified by
   * <b><i>length</i></b>.
   *
   * @param is the InputStream
   * @param length bytes to be consumed in the InputStream
   * @return ANSI decoded Java string
   * @throws IOException when an I/O error occurs
   */
  public static String readString(final InputStream is, final int length) throws IOException {
    final byte[] byteArray = new byte[length];
    final int bytesRead = is.read(byteArray);
    assert bytesRead == length;
    final int index = Bytes.indexOf(byteArray, (byte) 0);
    return new String(byteArray, 0, (index == -1) ? bytesRead : index, StandardCharsets.US_ASCII);
  }

  /**
   * Write a number of 0's into the given OutputStream
   *
   * @param os the OutputStream
   * @param length number of 0's to fill
   * @throws IOException when an I/O error occurs
   */
  public static void fillZero(final OutputStream os, final int length) throws IOException {
    final byte[] array = new byte[length];
    os.write(array);
  }
}
